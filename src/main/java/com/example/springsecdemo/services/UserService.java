package com.example.springsecdemo.services;

import com.example.springsecdemo.models.AppUser;
import org.springframework.stereotype.Service;

import java.util.Collection;

public interface UserService {
    AppUser findById(String uid);
    Collection<AppUser> findAll();
    AppUser add(String uid);
    void update(AppUser user);
    void delete(String uid);

}
