package com.example.springsecdemo.services;

import com.example.springsecdemo.models.AppUser;
import com.example.springsecdemo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public AppUser findById(String uid) {
        return userRepository.findById(uid).get();
    }

    @Override
    public Collection<AppUser> findAll() {
        return userRepository.findAll();
    }

    @Override
    public AppUser add(String uid) {
        AppUser newUser = new AppUser();
        newUser.setGuid(uid);
        return userRepository.save(newUser);
    }

    @Override
    public void update(AppUser user) {

    }

    @Override
    public void delete(String uid) {

    }
}
